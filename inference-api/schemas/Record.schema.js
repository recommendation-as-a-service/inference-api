const mongoose = require('mongoose');

module.exports = mongoose.model("Records", mongoose.Schema({
    url: String, //url to output data
    userId: String, //Id of the user who uploaded the data
    uploadId: String, //Id of the upload
    algorithm: String, //Algorithm of Data
    centroids: [
        [Number]
    ], //This will be None, unless it is kmeans
    matrixVUrl: String, //This will be None, unless it is matrix-decomp. It is the url to the second matrix
    _id: mongoose.SchemaTypes.ObjectId, //Id of the upload, ObjectId()
}));