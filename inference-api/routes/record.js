var express = require('express');
var router = express.Router();
const Record = require('../schemas/Record.schema');
const mongoose = require('mongoose');
const aws = require('aws-sdk');
var linearAlgebra = require('linear-algebra')(), // initialise it
    Vector = linearAlgebra.Vector,
    Matrix = linearAlgebra.Matrix;

aws.config.loadFromPath('./aws.config.json');
const s3 = new aws.S3();

router.get('/:userId', (req, res, next) => {
    Record.find({ userId: req.params.userId }, {}, (err, records) => {
        if (err) {
            res.status(500).json({
                message: "Unable to connect to DB",
                error: err
            });
        } else if (records == null || records.length == 0) {
            res.status(404).json({
                message: "No records were found with that Id",
                userId: req.params.userId
            });
        } else {
            res.json({
                records: records,
                message: "Found records for that user!"
            });
        }
    });
});

router.post('/kmeans/:id', (req, res, next) => {
    Record.findOne({ _id: req.params.id, algorithm: "kmeans" }, {}, (err, record) => {
        if (err) {
            res.status(500).json({
                message: "Unable to connect to DB",
                error: err
            });
        } else if (record == null) {
            res.status(404).json({
                message: "No record were found with that Id",
                id: req.params.id
            });
        } else {
            const centroids = record.centroids.map((centroid) => {
                return new Matrix(centroid);
            });
            const u = new Matrix(req.body.u);

            if (centroids[0].toArray()[0].length != u.toArray()[0].length) {
                res.status(400).json({
                    message: "Mismatch of Length",
                    expected: centroids[0].toArray()[0].length,
                    found: u.toArray()[0].length
                });
            } else {
                let distance = Math.sqrt(u.dot(centroids[0].trans()).data[0][0]);
                let closestIndex = 0;
                for (let i in centroids) {
                    if (Math.sqrt(u.dot(centroids[i].trans()).data[0][0]) < distance) {
                        closestIndex = i;
                    }
                }
                res.json({
                    centroid: centroids[closestIndex],
                    message: "Found the closest centroid"
                });
            }
        }
    });
});

router.post('/cosine/:id', (req, res, next) => {
    Record.findOne({ _id: req.params.id, algorithm: "cosine" }, {}, (err, record) => {
        if (err) {
            res.status(500).json({
                message: "Unable to connect to DB",
                error: err
            });
        } else if (record == null) {
            res.status(404).json({
                message: "No record were found with that Id",
                id: req.params.id
            });
        } else {
            const params = {
                Bucket: 'raas-final-project',
                Key: /(cosine[\S]+\.csv)/g.exec(record.url)[0]
            };
            s3.getObject(params, (err, data) => {
                if (err) {
                    res.status(500).json({
                        message: "There was an error getting csv",
                        error: err
                    });
                } else {
                    let csv = data.Body.toString('utf8');
                    let rows = csv.split("\n");
                    let U = rows.map(function(row) {
                        return new Matrix(row.split(","));
                    });

                    //let matrix = new Matrix(csvList);
                    let u = new Matrix(req.body.u);
                    res.json({
                        message: "Got CSV",
                        record: record,
                        result: U.map((row) => {
                            return row.trans().dot(u);
                        })
                    });
                }
            });
        }
    });
});


module.exports = router;